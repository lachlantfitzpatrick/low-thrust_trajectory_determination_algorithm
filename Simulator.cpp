//
// Created by lachlan on 22/04/17.
//

#include "Simulator.hpp"

Simulator::Simulator(Parameters *params) : params(params) {}

// Use the midpoint method to make a step. This will leave endState with all
// of its class members updated. Returns a vector of the thrust direction.
Vector Simulator::step(State *endState, Input *input) {
    // Time for a full step.
    double fullStep = params->simStep;
    // Time for a mid step.
    double halfStep = params->simStep / 2;
    // Calculate the intermediate initialState.
    State midState;
    midState.time = endState->time + halfStep;
    midState.x1 = endState->x1 + halfStep * endState->x2;
    midState.x2 = endState->x2 + halfStep * endState->x3;
    midState.x4 = endState->x4 + halfStep * endState->x5;
    midState.x5 = endState->x5 + halfStep * endState->x6;
    calculateTotalAcceleration(&midState, input);
    // Save the initial positions before the step.
    double initialX = endState->x1;
    double initialY = endState->x4;
    // Save the initial energy before the step.
    double initialE = endState->e;
    // Calculate the end initialState.
    endState->time = endState->time + fullStep;
    endState->x1 = endState->x1 + fullStep * midState.x2;
    endState->x2 = endState->x2 + fullStep * midState.x3;
    endState->x4 = endState->x4 + fullStep * midState.x5;
    endState->x5 = endState->x5 + fullStep * midState.x6;
    Vector thrust = calculateTotalAcceleration(endState, input);
    endState->calculateEnergy(params);
    endState->calculateMomentum();
    // Calculate the change in specific energy.
    Vector deltaPos(endState->x1 - initialX, endState->x4 - initialY);
    endState->calculateDeltaE(&deltaPos, &thrust, initialE);
    // Return the thrust vector.
    return thrust;
}

// Calculates the fuel of the given initialState.
double Simulator::calculateMass(State *state, double beta) {
    // Calculate the change in fuel.
    double deltaM = params->flowRate * params->simStep * beta;
    // Set the fuel of the initialState.
    state->fuel = state->fuel - deltaM;
    // Return the change in fuel.
    return deltaM;
}

// Calculates the acceleration of the satellite due to thrust.
Vector Simulator::calculateThrustAcceleration(
        State *state, Input *input) {
    // Calculate the new fuel of vState and record the change.
    double deltaM = calculateMass(state, input->beta);
    // Calculate the total fuel.
    double totalMass = state->fuel + params->mass;
    // Calculate the magnitude of the delta-v... rocket equation.
    double deltaV = params->vEx * log((totalMass + deltaM) / totalMass);
    // Calculate the direction that the spacecraft is moving in.
    double angle = input->alpha;
    // Calculate the thrust acceleration.
    Vector thrust(deltaV * cos(angle) / params->simStep,
                  deltaV * sin(angle) / params->simStep);
    // Return the thrust vector.
    return thrust;

}

// Calculates the total acceleration experienced by the satellite and returns
// the thrust that was applied.
Vector Simulator::calculateTotalAcceleration(State *state, Input *input) {
    // Calculate the distance.
    double r = state->calculateDistance();
    // Calculate the delta-v from the thrusters.
    Vector thrust = calculateThrustAcceleration(state, input);
    // Calculate the x acceleration.
    state->x3 = -state->x1 * params->mu / pow(r, 3) + thrust.x;
    // Calculate the y acceleration.
    state->x6 = -state->x4 * params->mu / pow(r, 3) + thrust.y;

    // Return the change in mass.
    return thrust;
}

// Use the midpoint method to simulate a trajectory for the duration of an
// input time step.
State Simulator::simulateInputStep(State *inState, Input *input) {
    // Initialise the initialState to return.
    State retState(inState);
    // Keep stepping until the time expires or the orbit is complete.
    Orbit orbit(&retState, params);
    double initialT = retState.time;
    while (retState.time < initialT + params->inStep &&
           !orbit.orbitComplete(params)) {
        step(&retState, input);
    }
    return retState;
}

// Use the midpoint method to simulate an entire trajectory from inputs.
State Simulator::simulateTrajectory(Inputs *inputs) {
    // Initialise the input.
    Input input = inputs->getInput(0);
    // Initialise a initialState.
    State state;
    state.initialiseState(params);
    Vector thrust = calculateTotalAcceleration(&state, &input);
    // Initialise the save stream if saving.
    // Check whether we are saving data.
    FILE *outputStream;
    if (params->saveOn) {
        // The file to save data to.
        outputStream = fopen(params->filename, "w");
        // Save the current initialState.
        state.printState(outputStream, thrust);

    }
    // Keep stepping until the time expires or the orbit is complete.
    Orbit orbit(&state, params);
    long outputTicks = 1;
    int thrustOutputTicks = 1;
    while (state.time < params->finalT &&
           !orbit.orbitComplete(params)) {
        // Perform a step.
        input = inputs->getInput(state.time);
        thrust = step(&state, &input);
        // Check we need to output.
        if (outputTicks * params->outStep > state.time && params->saveOn) {
            if (thrustOutputTicks > 1 / INPUT_T_SCALING) {
                state.printState(outputStream, thrust);
                outputTicks++;
                thrustOutputTicks = 0;
            } else {
                state.printState(outputStream);
                outputTicks++;
                thrustOutputTicks++;
            }
        }
    }
    // Check whether to close the output stream.
    if (params->saveOn) {
        // Close the output stream.
        fclose(outputStream);
    }
    // Return the final initialState.
    return state;
}

// Use the midpoint method to simulate an entire trajectory from the root node.
State Simulator::simulateTrajectory(Node *root) {
    // Initialise the input.
    Input input = root->input;
    // Initialise initialState.
    State state = root->initialState;
    Vector thrust = calculateTotalAcceleration(&state, &input);
    // Initialise the save stream if saving.
    // Check whether we are saving data.
    FILE *outputStream;
    if (params->saveOn) {
        // The file to save data to.
        outputStream = fopen(params->filename, "w");
        // Save the current initialState.
        state.printState(outputStream, thrust);

    }
    // Keep stepping until the next node arrives or the trajectory is finished.
    Node *next = root->getChildTrajectory();
    Node *current = root;
    Orbit orbit(&state, params);
    int thrustOutputTicks = 1;
    double lastOutputTime = 0;
    while (state.time < params->finalT && !orbit.orbitComplete(params)) {
        // Perform a step.
        input = current->input;
        thrust = step(&state, &input);
        // Check we need to output.
        if (lastOutputTime < state.time - params->outStep && params->saveOn) {
            if (thrustOutputTicks > 1 / INPUT_T_SCALING) {
                state.printState(outputStream, thrust);
                thrustOutputTicks = 0;
            } else {
                state.printState(outputStream);
                thrustOutputTicks++;
            }
            lastOutputTime = state.time;
        }
        // Check whether we need to move to the next node.
        if (state.time >= current->finalState.time) {
            // Check whether the accelerations are ever different.
            if (std::abs(state.x3) > std::abs(current->finalState.x3 * 1.1) ||
                std::abs(state.x3) < std::abs(current->finalState.x3 * 0.9)) {
                std::cout << "fek\n";
            }
            // This should mean that we are at the end of the simulation.
            if (next == nullptr) {
                break;
            }
            current = next;
            next = current->getChildTrajectory();
            // If the calculated state and the state of current are within
            // 1/1000 of each other then just use the current state.
//            if (abs(state.e) < abs(current->finalState.e * 1.001) &&
//                abs(state.e) > abs(current->finalState.e * 0.999)) {
            state = current->initialState;
//            } else {
//                std::cout << "Fark\n";
//            }
        }
    }
    // Check whether to close the output stream.
    if (params->saveOn) {
        // Close the output stream.
        fclose(outputStream);
    }
    // Return the final initialState.
    return state;
}

// Simulate an orbit using an initial state. This saves results that can be
// used to superimpose over the spiral trajectory to see where the satellite
// started and finished.
void Simulator::simulateOrbit(State *initial, char filename[FILENAME_MAX]) {
    // The state.
    State state(initial);
    // Reset the time.
    state.time = 0;
    // A passive input.
    Input input(0, 0, 0);
    // Open the output stream.
    FILE* outputStream = fopen(filename, "w");
    // Save the current state.
    state.printState(outputStream);
    // Determine the period.
    Orbit orbit(&state, params);
    double period = orbit.calculatePeriod();
    // Keep iterating until we reach the period.
    double lastOutputTime = 0;
    while (state.time <= period) {
        // Step the simulation.
        step(&state, &input);
        // Check whether we need to output.
        if (lastOutputTime < state.time - params->outStep) {
            // Save the data.
            state.printState(outputStream);
            // Increment lastOutputTime.
            lastOutputTime = state.time;
        }
    }
}

// Simulates an orbit using an orbit. This saves results that can be
// used to superimpose over the spiral trajectory to see where the satellite
// started and finished.
void Simulator::simulateOrbit(Orbit orbit, char *filename) {
    State state;
    state.initialiseState(&orbit, params);
    simulateOrbit(&state, filename);
}
