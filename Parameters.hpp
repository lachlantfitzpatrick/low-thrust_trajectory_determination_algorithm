//
// Created by lachlan on 22/04/17.
//

#ifndef RRT2_PARAMETERS_HPP
#define RRT2_PARAMETERS_HPP

#include <cstdio>
#include <math.h>
#include <string.h>
#include <fstream>
#include "State.hpp"

// Some useful conversions.
#define AU                      149.6e9 // Astronomical units.
#define KM                      1e3
#define SEC_TO_DAYS             1/60/60/24
#define SEC_PER_YEAR            60*60*24*365
#define RAD_TO_DEG              180/M_PI

// These are a few variables used to control the program.
#define DIST_SCALE              1/KM
#define THRUST_SCALE            1e6 // scales thrust vectors for plot
#define INPUT_T_SCALING         1e-2
#define GUESS_T_SCALING         0.75 // fraction of final time.
#define FINAL_T_SCALING         10
#define ORBIT_ACCURACY          0.05
#define LOWEST_ORBIT            0.01 // fraction of the lowest periapsis
#define ALPHA_SAMPLES           16
#define BETA_SAMPLES            4
#define EXPLORE                 0.4

struct Parameters {
    // Variables used to control the program.
    double initialPeriapsis = 6871 * KM; // distance of initial periapsis.
    double initialApoapsis = 6871 * KM; // distance of initial apoapsis.
    double finalPeriapsis = 6871 * KM; // distance of final periapsis.
    double finalApoapsis = 8971 * KM; // distance of final apoapsis.
    double maxTime = SEC_PER_YEAR * 5;
    double guessTime;

    // Simulation parameters.
    double simStep; // simulation time step.
    double inStep; // input time step.
    double outStep; // output time step.
    double optStep; // optimisation step.
    double finalT; // termination time of the simulation.
    double initialE; // initial energy of the satellite.
    double initialH; // initial angular momentum of the satellite.
    char filename[FILENAME_MAX]; // name of the output file.
    double mu = 3.986e14; // orbital body gravitation parameter (GM).
    double mass = 61639; // dry mass of the satellite.
    double initialFuelMass = 2e4; // initial fuel of fuel.
    double flowRate = 6.7382e-6; // flow rate of the fuel. // xr-5: 0.0067382e-3
    double vEx = 1.73637e4; // exhaust velocity of the fuel. // xr-5: 17363.7
    double angleRate = 2 * M_PI / (60 * 60); // attitude control: rad/s.
    bool saveOn = false; // flag to indicate if we should save or not.
    double finalSpecificE = -mu / (finalApoapsis + finalPeriapsis);

    Parameters(std::ifstream *file);

    double calculateFuelTankMass(double initialFuelMass);
};


#endif //RRT2_PARAMETERS_HPP
