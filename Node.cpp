//
// Created by lachlan on 23/04/17.
//

#include "Node.hpp"

// Constructor for the root node.
Node::Node(State *initialState, long id, Input input, Parameters *params,
           Simulator *sim) : id(id), input(input), initialState(initialState),
                             orbit(Orbit(0, 0, params)), params(params) {
    isRoot = true;
    parent = nullptr;
    finalState = sim->simulateInputStep(initialState, &input);
    orbit = Orbit(&finalState, params);
    value = calculateValue();
}

// Constructor for normal nodes.
Node::Node(State *initialState, long id, Node *parent, Input input,
           Parameters *params, Simulator *sim) :
        id(id), parent(parent), input(input), orbit(Orbit(0, 0, params)),
        initialState(initialState), params(params) {
    depth = parent->depth + 1;
    finalState = sim->simulateInputStep(initialState, &input);
    orbit = Orbit(&finalState, params);
    value = calculateValue();
}

// Check whether this node is finished.
bool Node::isComplete() {
    // Return whether the orbit is complete.
    return orbit.orbitComplete(params);
}

// Calculate the value of this according to how close the periapsis and
// apoapsis are to finishing.
double Node::calculateValue() {
    // Calculate the difference between the current orbit and the desired
    // orbit. Square the result so that there is a stronger incentive to move
    // both the further axis towards the goal.
    double value = -pow(std::abs(params->finalApoapsis - orbit.apoapsis), 2);
    value += -pow(std::abs(params->finalPeriapsis - orbit.periapsis), 2);
    return value;
}

// Adds a child to this.
void Node::addChild(Node *child) {
    children.push_back(child);
}

// Gets the child of this that is in the trajectory.
Node *Node::getChildTrajectory() {
    // Iterate over all the children.
    for (std::vector<Node *>::iterator it = children.begin();
         it != children.end(); it++) {
        // If the child is in the trajectory return it.
        if ((*it)->isTrajectory) {
            return *it;
        }
    }
    return nullptr;
}

// Iterate up the tree until root is found.
Node *Node::getRoot() {
    if (isRoot) {
        return this;
    } else if (parent == nullptr) {
        std::cout << "No root!!\n";
        return nullptr;
    } else {
        return parent->getRoot();
    }
}

// Indicates that the current is part of the finished trajectory and updates
// the parent accordingly.
void Node::finishTree() {
    isTrajectory = true;
    if (!isRoot) {
        parent->finishTree();
    }
}

// Generates the inputs to go from this node to the end.
Inputs Node::generateInputs() {
    // Iterate over all of the nodes until there are no more and add them to
    // the list.
    std::vector<Input> inputs;
    Node *next = this;
    while (next != nullptr) {
        inputs.push_back(next->input);
        next = next->getChildTrajectory();
    }
    return Inputs(std::vector<Input>());
}
