//
// Created by lachlan on 22/04/17.
//

#ifndef RRT2_SIMULATOR_HPP
#define RRT2_SIMULATOR_HPP


#include "State.hpp"
#include "Inputs.hpp"
#include "Vector.hpp"
#include "Node.hpp"

struct Node;

class Simulator {

    Parameters *params;

    Vector step(State *endState, Input *input);

    double calculateMass(State *state, double beta);

    Vector calculateThrustAcceleration(State *state,
                                       Input *input);

    Vector calculateTotalAcceleration(State *state, Input *input);

public:

    Simulator(Parameters *params);

    State simulateInputStep(State *inState, Input *input);

    State simulateTrajectory(Inputs *inputs);

    State simulateTrajectory(Node *root);

    void simulateOrbit(Orbit orbit, char filename[FILENAME_MAX]);

    void simulateOrbit(State *initial, char *filename);
};


#endif //RRT2_SIMULATOR_HPP
