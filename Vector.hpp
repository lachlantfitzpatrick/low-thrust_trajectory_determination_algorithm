//
// Created by lachlan on 22/04/17.
//

#ifndef RRT2_VECTOR_HPP
#define RRT2_VECTOR_HPP

struct Vector {
    double x;
    double y;

    Vector(double x, double y) : x(x), y(y) {}

    double mag() {
        return sqrt(pow(x, 2) + pow(y, 2));
    }
};

#endif //RRT2_VECTOR_HPP
