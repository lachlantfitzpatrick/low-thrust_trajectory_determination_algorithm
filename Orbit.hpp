//
// Created by lachlan on 22/04/17.
//

#ifndef RRT2_ORBIT_HPP
#define RRT2_ORBIT_HPP

#include <cmath>
#include "State.hpp"
#include "Parameters.hpp"

class Parameters;

class State;

struct Orbit {
    double apoapsis;
    double periapsis;
    Parameters *params;

    Orbit(State *state, Parameters *params);

    Orbit(double apoapsis, double periapsis, Parameters *params);

    double calculateAxis();

    double calculatePeriod();

    double calculateEnergy(Parameters *params);

    bool orbitComplete(Parameters *params);
};

#endif //RRT2_ORBIT_HPP
