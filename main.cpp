#include <iostream>
#include <fstream>
#include <ctime>

#include "Inputs.hpp"
#include "Simulator.hpp"
#include "RRT.hpp"

#define PARAMS_FILE                 "interplanetary_parameters.txt"

// Override for printing Input.
std::ostream &operator<<(std::ostream &strm, const Input &input) {
    return input.dump(strm);
}

// Override for printing std::vector<double>.
std::ostream &operator<<(std::ostream &strm, const std::vector<double> &vec) {
    for (std::vector<double>::const_iterator it = vec.begin();
         it != vec.end(); it++) {
        strm << *it << "," << *(it++) << "\n";
    }
    return strm;
}

// Runs the program.
int main() {
    // Generate and initialise the parameters.
    std::ifstream paramsFile(PARAMS_FILE);
    Parameters params(&paramsFile);
    // Calculate the period.
    Orbit orbit(params.initialPeriapsis, params.initialApoapsis, &params);
    double period = orbit.calculatePeriod();
    // Set times based on the period.
    params.simStep = period * 1e-3;
    params.outStep = period * INPUT_T_SCALING;
    params.inStep = period * 1e-1;
    params.finalT = period * FINAL_T_SCALING;
    params.guessTime = params.finalT * GUESS_T_SCALING;
    strcpy(params.filename, "results.csv");
    // Set the filenames to save the initial and final orbits to.
    char initialFilename[FILENAME_MAX] = "initial_orbit.csv";
    char finalFilename[FILENAME_MAX] = "final_orbit.csv";
    char moonFilename[FILENAME_MAX] = "moon_orbit.csv";
    // Instantiate a simulator.
    Simulator simulator(&params);
    // Generate the root node.
    Input input(0, 0, 0);
    State state;
    state.initialiseState(&params);
    Node root(&state, 0, input, &params, &simulator);
    // Create an instance of the RRT algorithm.
    RRT rrt(&params, &simulator);
    Inputs inputs = rrt.findTrajectory(&root);
    // Wait for confirmation to finish saving the trajectory.
    std::cout << "Finish?\n";
    char tempStr[100];
    fgets(tempStr, 100, stdin);
    std::cout << "Finishing\n";
    // Simulate the trajectory.
//    Inputs inputs = root.generateInputs();
    params.saveOn = true;
    clock_t begin = clock();
    State res = simulator.simulateTrajectory(&root);
    clock_t end = clock();
    std::cout << "Runtime: " << (end - begin) / 1e3 << "\n"; // milliseconds
    res.printState(&params);
    // Simulate the other orbits.
    State initialState;
    initialState.initialiseState(&params);
    simulator.simulateOrbit(&initialState, initialFilename);
    simulator.simulateOrbit(&res, finalFilename);
    simulator.simulateOrbit(Orbit(params.finalApoapsis, params.finalApoapsis,
                                  &params), moonFilename);
    return 0;
}

//// Tests the inputs.
//int main() {
//    // Test construction.
//    Input i1(0, 1, 1);
//    Input i2(5, 2, 2);
//    Input i3(100.11, 3, 3);
//    std::vector<Input> is;
//    is.push_back(i1);
//    is.push_back(i2);
//    is.push_back(i3);
//    Inputs inputs(is);
//    std::cout << inputs.getInput(1) << "\n";
//    std::cout << inputs.getInput(2) << "\n";
//    std::cout << inputs.getInput(5.00000000001) << "\n";
//    std::cout << inputs.getInput(100.1) << "\n";
//
//    // Test converting to and from optimisation format.
//    std::ifstream paramsFile(PARAMS_FILE);
//    Parameters params(&paramsFile);
//    params.optStep = 1;
//    params.finalT = 102;
//    std::vector<double> optInputs = inputs.getOptimisationInputs(&params);
//    std::cout << optInputs << "\n";
//    inputs.setOptimisationInputs(&optInputs, &params);
//    std::cout << inputs.getInput(1) << "\n";
//    std::cout << inputs.getInput(2) << "\n";
//    std::cout << inputs.getInput(5.00000000001) << "\n";
//    std::cout << inputs.getInput(100.1) << "\n";
//    return 0;
//}

//// Test the simulator.
//int main() {
//    // Generate and initialise the parameters.
//    std::ifstream paramsFile(PARAMS_FILE);
//    Parameters params(&paramsFile);
//    // Calculate the period.
//    Orbit orbit(params.initialPeriapsis, params.initialApoapsis, &params);
//    double period = orbit.calculatePeriod();
//    // Set times based on the period.
//    params.simStep = period * 1e-4;
//    params.outStep = period * INPUT_T_SCALING;
//    params.inStep = period * 1e-1;
//    params.finalT = period * FINAL_T_SCALING;
//    params.guessTime = params.finalT * GUESS_T_SCALING;
//    strcpy(params.filename, "results.csv");
//    // Instantiate a simulator.
//    Simulator simulator(&params);
//    // Generate an input that does nothing.
//    Input input(0, 0, 1);
//    std::vector<Input> temp;
//    temp.push_back(input);
//    // Generate the inputs.
//    Inputs inputs(temp);
//    // Simulate the trajectory.
//    params.saveOn = true;
//    clock_t begin = clock();
//    State res = simulator.simulateTrajectory(&inputs);
//    clock_t end = clock();
//    std::cout << "Runtime: " << (end - begin) / 1e3 << "\n"; // milliseconds
//    res.printState(&params);
//    return 0;
//}