//
// Created by lachlan on 23/04/17.
//

#include "Parameters.hpp"

// Initialises a parameters object using the contents of a file.
Parameters::Parameters(std::ifstream *file) {
    // First line is initial periapsis.
    *file >> initialPeriapsis;
    // Second line is initial apoapsis.
    *file >> initialApoapsis;
    // Third line is final periapsis.
    *file >> finalPeriapsis;
    // Fourth line is final apoapsis.
    *file >> finalApoapsis;
    // Fifth line is gravitational parameters mu.
    *file >> mu;
    // Sixth line is payload mass.
    *file >> mass;
    // Seventh line is initial fuel mass.
    *file >> initialFuelMass;
    // Eighth line is exhaust velocity.
    *file >> vEx;
    // Ninth line is force which is then converted to flow rate.
    double force;
    *file >> force;
    flowRate = force / vEx;

    // Calculate the mass due to the fuel.
    mass += calculateFuelTankMass(initialFuelMass);
    // Calculate the final specific energy.
    finalSpecificE = -mu / (finalApoapsis + finalPeriapsis);
}

// Calculates the mass of the fuel tank required given the mass of the fuel.
double Parameters::calculateFuelTankMass(double initialFuelMass) {
    return initialFuelMass / 450 * 22.26;
}
