//
// Created by lachlan on 22/04/17.
//

#include "Orbit.hpp"


Orbit::Orbit(State *state, Parameters *params) : params(params) {
    // For very high orbits the rounding errors present in doubles may be
    // enough to cause an error below.
    double e2 = 1 + 2 * state->e * pow(state->h, 2) / pow(params->mu, 2);
    e2 = std::abs(e2) < 1e-20 ? 0 : e2;
    // Calculate the eccentricity.
    double e = sqrt(e2);
    // Calculate the apoapsis.
    apoapsis = pow(state->h, 2) / (params->mu * (1 + e * cos(M_PI)));
    // Calculate the periapsis.
    periapsis = pow(state->h, 2) / (params->mu * (1 + e * cos(0)));
}

Orbit::Orbit(double apoapsis, double periapsis, Parameters *params) :
        apoapsis(apoapsis), periapsis(periapsis), params(params) {}

double Orbit::calculateAxis() {
    return (apoapsis + periapsis) / 2;
}

double Orbit::calculatePeriod() {
    return 2 * M_PI * sqrt(pow(calculateAxis(), 3) / params->mu);
}

double Orbit::calculateEnergy(Parameters *params) {
    return -params->mu / (apoapsis + periapsis);
}

// Checks whether orbit is the same as the final orbit within the accuracy
// bounds. Returns true if orbit is within bounds.
bool Orbit::orbitComplete(Parameters *params) {
    // Check if apoapsis is within bounds.
    if ((apoapsis > (params->finalApoapsis * (1 + ORBIT_ACCURACY))) ||
        (apoapsis < (params->finalApoapsis * (1 - ORBIT_ACCURACY)))) {
        return false;
    }
    // Check if periapsis is within bounds.
    if ((periapsis > (params->finalPeriapsis * (1 + ORBIT_ACCURACY))) ||
        (periapsis < (params->finalPeriapsis * (1 - ORBIT_ACCURACY)))) {
        return false;
    }
    return true;
}
