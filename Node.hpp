//
// Created by lachlan on 23/04/17.
//

#ifndef RRT2_NODE_HPP
#define RRT2_NODE_HPP


#include "State.hpp"
#include "Inputs.hpp"
#include "Simulator.hpp"
#include <iostream>

class Simulator;

struct Node {
    bool expanded = false;
    State initialState; // the initial state of this node
    State finalState; // the final state of this node
    long id; // Unique identifier
    double value; // value of this according to some heuristic
    Node *parent;
    Input input;
    bool isRoot = false;
    int depth = 0;
    Parameters *params;
    Orbit orbit;
    std::vector<Node *> children;
    bool isTrajectory = false;

    Node(State *initialState, long id, Input input, Parameters *params,
         Simulator *sim);

    Node(State *initialState, long id, Node *parent, Input input,
         Parameters *params, Simulator *sim);

    bool isComplete();

    double calculateValue();

    void addChild(Node *node);

    Node *getChildTrajectory();

    Node *getRoot();

    void finishTree();

    Inputs generateInputs();

};

// Defines a comparator for a Node.
struct CompareNode {
    bool operator()(const Node *lhs, const Node *rhs) const {
        return lhs->value < rhs->value;
    }
};


#endif //RRT2_NODE_HPP
