//
// Created by lachlan on 22/04/17.
//

#ifndef RRT2_INPUTS_HPP
#define RRT2_INPUTS_HPP

#include <vector>
#include <ostream>
#include "Parameters.hpp"

// Defines a struct that contains an alpha and beta to be used as input.
typedef struct Input {
    double alpha;
    double beta;
    double time;

    Input(double time, double alpha, double beta) : time(time), alpha(alpha),
                                                    beta(beta) {}

    std::ostream &dump(std::ostream &strm) const {
        return strm << "time: " << time << ", alpha: " << alpha
                    << ", beta: " << beta;
    }
} Input;


class Inputs {

    std::vector<Input> inputs;

public:

    Inputs(std::vector<Input> inputs);

    Input getInput(double time);

    std::vector<double> getOptimisationInputs(Parameters *params);

    void setOptimisationInputs(std::vector<double> *inputs, Parameters *params);

};


#endif //RRT2_INPUTS_HPP
