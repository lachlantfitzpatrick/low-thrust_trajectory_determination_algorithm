//
// Created by lachlan on 22/04/17.
//

#ifndef RRT2_RRT_HPP
#define RRT2_RRT_HPP

#include <random>
#include <unordered_map>
#include <queue>
#include "Node.hpp"
#include "Simulator.hpp"

class RRT {

    Simulator *sim;
    std::random_device rd;
    std::mt19937 gen;
    long ids;
    std::priority_queue<Node *, std::vector<Node *>, CompareNode> pq;
    std::unordered_map<long, Node *> hm;
    Parameters *params;

public:

    RRT(Parameters *params, Simulator *sim);

    void addFringe(Node *node);

    Inputs findTrajectory(Node *root);

    Node *selectFringe(double strategy);

    void expandFringe(Node *selected);

    Inputs generateInputs(Node *fringe);

};


#endif //RRT2_RRT_HPP
