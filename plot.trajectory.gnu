#!/usr/bin/env gnuplot

# Variables
dataFolder = "./cmake-build-debug/"

# Settings
load "settings.gnu"

# Output
set terminal png size 1200,1200 font "Helvetica Neue Light,16"
set output "plot.trajectory.png"

# Key
set key outside center bottom horizontal

# Labels
set title "Enyo SEP Tug Trajectory" font "Helvetica Neue Light,24"
set xlabel "x (km)"
set ylabel "y (km)"

# Axes
set tics in
set ytics nomirror
set grid xtics
set grid ytics
set size ratio -1
show grid

# Plot
set multiplot
plot sprintf("%s%s", dataFolder, "results.csv") using 2:3 with lines ls 8 \
    title "Trajectory", \
     sprintf("%s%s", dataFolder, "moon_orbit.csv") using 2:3 with lines ls 5 \
     title "Lunar Orbit", \
     sprintf("%s%s", dataFolder, "initial_orbit.csv") using 2:3 \
     with lines ls 7 title "Initial Orbit", \
     sprintf("%s%s", dataFolder, "final_orbit.csv") using 2:3 \
     with lines ls 6 title "Final Orbit"

       #sprintf("%s%s", dataFolder, "results.csv") using 2:3:8:9 with vector \
        #nohead ls 4 title "Thrust Vector", \
