# This code comes courtesy of Tom Hines.
	
# Line styles
set style line 1 lt 1 lw 1 pt 1 ps 1 lc rgb "#EDB75C"
set style line 2 lt 1 lw 1 pt 1 ps 1 lc rgb "#645B4B"
set style line 3 lt 1 lw 1 pt 1 ps 1 lc rgb "#D94023"
set style line 4 lt 1 lw 1 pt 1 ps 1 lc rgb "#0000ff" # blue
set style line 5 lt 1 lw 3 pt 1 ps 1 lc rgb "#a6bcbb" # moon grey
set style line 6 lt 1 lw 3 pt 1 ps 1 lc rgb "#35c625" # lime green
set style line 7 lt 1 lw 3 pt 1 ps 1 lc rgb "#27cbce" # sky blue
set style line 8 lt 1 lw 1 pt 1 ps 1 lc rgb "#ff7f00" # orange
set style line 9 lt 1 lw 3 pt 1 ps 1 lc rgb "#ff0000" # red

# Delimiter
set datafile separator ","

