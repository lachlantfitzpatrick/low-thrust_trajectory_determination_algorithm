//
// Created by lachlan on 22/04/17.
//

#include <iostream>
#include "State.hpp"

double State::calculateDistance() {
    return sqrt(pow(x1, 2) + pow(x4, 2));
}

double State::calculateAnomaly() {
    return atan2(x4, x1);
}

double State::calculateSpeed() {
    return sqrt(pow(x2, 2) + pow(x5, 2));
}

// Updates the specific energy and returns it.
double State::calculateEnergy(Parameters *params) {
    e = (0.5 * pow(calculateSpeed(), 2) - params->mu / calculateDistance());
    return e;
}

// Updates the specific momentum and returns it.
double State::calculateMomentum() {
    // Calculate the cross product between position and velocity.
    h = (x1 * x5 - x2 * x4);
    return h;
}

// Calculates the change in total change in specific energy that has been
// applied due to the thrusters.
void State::calculateDeltaE(Vector *deltaPos, Vector *thrust,
                            double initialE) {
    thrusterDeltaE += deltaPos->mag() + thrust->mag();
    orbitDeltaE += std::abs(initialE - e);
}

// Sets this initialState as if it was the first initialState in a simulation.
void State::initialiseState(Parameters *params) {
    time = 0;
    x1 = params->initialApoapsis;
    x2 = 0;
    x4 = 0;
    Orbit orbit(params->initialApoapsis, params->initialPeriapsis,
                params);
    x5 = sqrt(params->mu * (2 / params->initialApoapsis -
                            1 / orbit.calculateAxis()));
    fuel = params->initialFuelMass;
    // Calculate the energy and momentum and set the initial values of params
    // to this.
    params->initialE = calculateEnergy(params);
    params->initialH = calculateMomentum();
    thrusterDeltaE = 0;
    orbitDeltaE = 0;
}

// Sets this initialState as if it was the first initialState in a simulation.
void State::initialiseState(Orbit *orbit, Parameters *params) {
    time = 0;
    x1 = orbit->apoapsis;
    x2 = 0;
    x4 = 0;
    x5 = sqrt(params->mu * (2 / orbit->apoapsis - 1 / orbit->calculateAxis()));
    fuel = params->initialFuelMass;
    // Calculate the energy and momentum and set the initial values of params
    // to this.
    params->initialE = calculateEnergy(params);
    params->initialH = calculateMomentum();
    thrusterDeltaE = 0;
    orbitDeltaE = 0;
}

// Prints to a file stream the initialState.
// initialState: the initialState to print.
// file: the file stream to print to.
void State::printState(FILE *stream) {
    // Calculate the distance.
    double r = calculateDistance();
    // Calculate the anomaly in degrees.
    double anomaly = calculateAnomaly();
    // Print the initialState. Format of the file is: time, x-pos, y-pos, x-vel, y-vel.
    fprintf(stream, "%.10lf,%.10lf,%.10lf,%.10lf,%.10lf,%.10lf,%.10lf\n",
            time * SEC_TO_DAYS, x1 * DIST_SCALE,
            x4 * DIST_SCALE, thrusterDeltaE, orbitDeltaE, r * DIST_SCALE,
            anomaly * RAD_TO_DEG);
}

// Prints to a file stream the initialState.
// initialState: the initialState to print.
// file: the file stream to print to.
void State::printState(FILE *stream, Vector thrust) {
    // Calculate the distance.
    double r = calculateDistance();
    // Calculate the anomaly in degrees.
    double anomaly = calculateAnomaly();
    // Print the initialState.
    fprintf(stream,
            "%.10lf,%.10lf,%.10lf,%.10lf,%.10lf,%.10lf,%.10lf,%.10lf,%.10lf,%.10lf\n",
            time * SEC_TO_DAYS,
            x1 * DIST_SCALE, x4 * DIST_SCALE,
            thrusterDeltaE, orbitDeltaE,
            r * DIST_SCALE, anomaly * RAD_TO_DEG,
            -thrust.x * THRUST_SCALE, -thrust.y * THRUST_SCALE,
            atan2(thrust.y, thrust.x));
}

void State::printState(Parameters *params) {
    // Determine the orbit.
    Orbit orbit(this, params);
    std::cout << "Periapsis: " << orbit.periapsis
              << "; Apoapsis: " << orbit.apoapsis << "\n";
    std::cout << "Orbit complete: " << orbit.orbitComplete(params) << "\n";
    std::cout << "Fuel used: " << params->initialFuelMass - fuel << "\n";
    std::cout << "Specific Energy: " << e << "\n";
    std::cout << "Final time: " << params->finalT * SEC_TO_DAYS << "\n";
    std::cout << "Initial mass: " << params->mass << "\n";
    std::cout << "Initial fuel: " << params->initialFuelMass << "\n";
}

// Checks whether a initialState is allowed.
bool State::stateAllowed(Parameters *params) {
    // Check the fuel limit hasn't been exceeded.
    if (fuel <= 0) {
        return false;
    }
    // Check that the time limit hasn't been exceeded.
    if (time > params->maxTime) {
        return false;
    }
    // Ensure that the craft doesn't go below the lowest periapsis.
    Orbit orbit(this, params);
    double lowestPeriapsis =
            params->finalApoapsis < params->initialPeriapsis ?
            params->finalPeriapsis : params->initialPeriapsis;
    if (orbit.periapsis < (lowestPeriapsis * (1 - LOWEST_ORBIT))) {
        return false;
    }
    // Otherwise we are good.
    return true;
}

