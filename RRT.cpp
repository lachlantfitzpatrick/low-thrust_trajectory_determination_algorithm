//
// Created by lachlan on 22/04/17.
//

#include "RRT.hpp"

// Constructs an instance of RRT.
RRT::RRT(Parameters *params, Simulator *sim) :
        params(params), gen(rd()), ids(1), sim(sim) {}

// Performs a search using a RRT-like algorithm. At the end of the algorithm
// the root will contain all the information about the trajectory.
Inputs RRT::findTrajectory(Node *root) {
    // Initialise the random number generator for selecting a strategy.
    std::uniform_real_distribution<> selectStrategy(0, 1);
    // Initialise the IDs.
    ids = root->id;
    // Clear the priority queue and hash map.
    pq = std::priority_queue<Node *, std::vector<Node *>, CompareNode>();
    hm.clear();
    // Add the root as a fringe.
    addFringe(root);
    // Keep searching until we find a solution.
    Node *selected;
    while (true) {
        // Select the fringe to expand from.
        selected = selectFringe(selectStrategy(gen));
        // Print the current orbit.
        std::cout << "Periapsis: " << selected->orbit.periapsis
                  << "; Apoapsis: " << selected->orbit.apoapsis
                  << "; Fuel left: " << selected->finalState.fuel
                  << "; Time: " << selected->finalState.time*SEC_TO_DAYS
                  << "\n";
        // Check if the trajectory is finished.
        if (selected->isComplete()) {
            // Update the final time.
            params->finalT = selected->finalState.time;
            // Print the final initialState.
            selected->finalState.printState(params);
            // Update the tree to identify the trajectory path.
            selected->finishTree();
            // Exit function.
            return generateInputs(selected);
        }
        // Expand from selected provided it doesn't exceed limits.
        if (selected->finalState.stateAllowed(params)) {
            // Expand from the selected fringe.
            expandFringe(selected);
        } else {
            // Otherwise indicate that it was expanded then resample.
            selected->expanded;
        }
    }
}

// Adds a fringe.
void RRT::addFringe(Node *node) {
    pq.push(node);
    hm.insert(std::make_pair(node->id, node));
}

// Selects a fringe from either the priority queue or the hash map.
Node *RRT::selectFringe(double strategy) {
    // Use the given strategy.
    if (strategy < EXPLORE) {
        // Explore strategy.
        // Generate a random number to index the hash map.
        std::uniform_int_distribution<long> randomIndex(0, hm.size() - 1);
        // Get the iterator for the hash map.
        std::unordered_map<long, Node *>::iterator it = hm.begin();
        // Advance the iterator to a random point in the hash map.
        std::advance(it, randomIndex(gen));
        // Use the leaf at the new position of it.
        Node *selected = (&*it)->second;
        // Keep popping elements until one is removed that has not been expanded.
        while (selected->expanded) {
            // Remove selected from the hash map.
            hm.erase(selected->id);
            // Get the iterator for the hash map.
            it = hm.begin();
            // Advance the iterator to a random point in the hash map.
            std::advance(it, randomIndex(gen));
            // Use the leaf at the new position of it.
            selected = it->second;
        }
        // Remove selected from the hash map.
        hm.erase(selected->id);
        // Return selected.
        return selected;
    } else {
        // Exploit strategy.
        // Get the first element in the priority queue.
        Node *selected = pq.top();
        // Remove the first element in the priority queue.
        pq.pop();
        // Keep popping elements until one is removed that has not been
        // expanded.
        while (selected->expanded) {
            selected = pq.top();
            pq.pop();
        }
        // Remove selected from the hash map.
        hm.erase(selected->id);
        // Return selected.
        return selected;
    }
}

// Expands a fringe using a series of strategies.
void RRT::expandFringe(Node *selected) {
    // Expand selected.
    selected->expanded = true;
    // There are several nodes that will be expanded from this. Initialise
    // them all.
    Input dull(selected->finalState.time + params->inStep, 0, 0);
    State passive = sim->simulateInputStep(&selected->finalState, &dull);
    Node *apoNode = new Node(&passive, ids++, selected, dull, params, sim);
    Node *periNode = new Node(&passive, ids++, selected, dull, params, sim);
    Node *energyNode = new Node(&passive, ids++, selected, dull, params, sim);
    // The allowed change in angle.
    double deltaTheta = params->inStep * params->angleRate;
    for (int j = 0; j <= BETA_SAMPLES; j++) {
        // Generate all of the sample states.
        for (int i = 0; i < ALPHA_SAMPLES; i++) {
            // Generate the input.
            Input input(selected->finalState.time, i * 2 * M_PI / ALPHA_SAMPLES,
                        j / BETA_SAMPLES);
            // Check that this angle is within the allowed angle change.
            if (abs(input.alpha - selected->input.alpha) <= deltaTheta) {
                // Perform the simulation.
                Node *sample = new Node(&selected->finalState, ids++, selected,
                                        input, params, sim);
                // Check if this orbit is the best apoapsis thus far.
                if ((abs(sample->orbit.apoapsis - params->finalApoapsis) <
                     abs(apoNode->orbit.apoapsis - params->finalApoapsis))) {
                    // Delete the current node at apoNode.
                    delete apoNode;
                    // Set apoNode to sample.
                    apoNode = sample;
                }
                    // Check if this orbit is the best periapsis thus far.
                else if ((
                        abs(sample->orbit.periapsis - params->finalPeriapsis) <
                        abs(periNode->orbit.periapsis -
                            params->finalPeriapsis))) {
                    // Delete the current node at periNode.
                    delete periNode;
                    // Set periNode to sample.
                    periNode = sample;
                }
                    // Check if this orbit is the best energy thus far.
                else if ((abs(sample->finalState.e - params->finalSpecificE)) <
                         abs(energyNode->finalState.e -
                             params->finalSpecificE)) {
                    // Delete the current node at energyNode.
                    delete energyNode;
                    // Set energyNode to sample.
                    energyNode = sample;
                }
                    // Sample wasn't used so delete it.
                else {
                    delete sample;
                    sample = nullptr;
                }
            }
        }
    }
    // Add the children.
    selected->addChild(apoNode);
    selected->addChild(periNode);
    selected->addChild(energyNode);
    // Add the fringes.
    addFringe(apoNode);
    addFringe(periNode);
    addFringe(energyNode);
}

// Generate the inputs.
Inputs RRT::generateInputs(Node *fringe) {
    // Temporary vector to store the inputs in.
    std::vector<Input> temp;
    std::cout << "Solution depth: " << fringe->depth << "\n";
    // Iterate up the tree.
    int counter = 0;
    while (!fringe->isRoot) {
        // Add the current alpha input.
        temp.push_back(fringe->input);
        // Always check that the parent's depth is one less than current's.
        if (fringe->depth - 1 != fringe->parent->depth) {
            std::cout << "fuck\n";
        }
        // Use the parent as the new fringe.
        fringe = fringe->parent;
        counter++;
    }
    // Add the input of root in.
    temp.push_back(fringe->input);
    std::cout << "Number of input: " << counter << "\n";
    // Reverse the order of temp.
    std::vector<Input> actual;
    for (std::vector<Input>::reverse_iterator it = temp.rbegin();
         it != temp.rend(); it++) {
        actual.push_back(*it);
    }
    // Set the inputs of inputs.
    Inputs inputs(actual);
    // Return inputs.
    return inputs;
}