//
// Created by lachlan on 22/04/17.
//

#ifndef RRT2_STATE_HPP
#define RRT2_STATE_HPP

#include "Parameters.hpp"
#include "Vector.hpp"
#include "Orbit.hpp"
#include <cmath>

class Parameters;

class Orbit;

struct State {
    double time; // simulation time.
    double x1; // x position.
    double x2; // x velocity.
    double x3; // x acceleration.
    double x4; // y position.
    double x5; // y velocity.
    double x6; // y acceleration.
    double fuel; // amount of fuel.
    double e; // specific energy.
    double h; // specific angular momentum.
    // the total change in specific energy that has been applied due to
    // thrusters.
    double thrusterDeltaE;
    // the total change in specific energy that has resulted from orbit changes.
    double orbitDeltaE;

    State(double x1, double x2, double x4, double x5) :
            x1(x1), x2(x2), x4(x4), x5(x5) {}

    State() : time(0), x1(0), x2(0), x3(0), x4(0), x5(0), x6(0), fuel(0), e(0),
              h(0), thrusterDeltaE(0), orbitDeltaE(0) {}

    State(State *state) : time(state->time), x1(state->x1), x2(state->x2),
                          x3(state->x3), x4(state->x4), x5(state->x5),
                          x6(state->x6), fuel(state->fuel), e(state->e),
                          h(state->h), thrusterDeltaE(state->thrusterDeltaE),
                          orbitDeltaE(state->orbitDeltaE) {}

    double calculateDistance();

    double calculateAnomaly();

    double calculateSpeed();

    double calculateEnergy(Parameters *params);

    double calculateMomentum();

    void initialiseState(Parameters *params);

    void calculateDeltaE(Vector *deltaPos, Vector *thrust, double initialE);

    void printState(FILE *stream);

    void printState(Parameters *params);

    void printState(FILE *stream, Vector thrust);

    bool stateAllowed(Parameters *params);

    void initialiseState(Orbit *orbit, Parameters *params);
};

#endif //RRT2_STATE_HPP
