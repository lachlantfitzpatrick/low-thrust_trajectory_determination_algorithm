//
// Created by lachlan on 22/04/17.
//

#include "Inputs.hpp"

// Constructs an instance of this. Assumes that the inputs are already in
// chronological order.
Inputs::Inputs(std::vector<Input> inputs) : inputs(inputs) {}

// Gets the input associated with the given time.
Input Inputs::getInput(double time) {
    // Iterate over all the inputs.
    for (int i = 0; i < inputs.size() - 1; i++) {
        // Check whether the times are correct.
        if (inputs[i].time <= time && inputs[i + 1].time > time) {
            return inputs[i];
        }
    }
    return inputs[inputs.size() - 1];
}

// Converts the inputs into the format for an optimisation. Assumes that
// finalT has been set in params.
std::vector<double> Inputs::getOptimisationInputs(Parameters *params) {
    // Data to return.
    std::vector<double> ret;
    // Iterate over all the inputs and populate the ret vector.
    Input input = getInput(0);
    for (double t = 0; t < params->finalT; t += params->optStep) {
        input = getInput(t);
        ret.push_back(input.alpha);
        ret.push_back(input.beta);
    }
    return ret;
}

// Takes inputs that were generated from an optimisation and puts them into
// the format of this.
void Inputs::setOptimisationInputs(std::vector<double> *inputs,
                                   Parameters *params) {
    this->inputs.clear();
    // Iterate over the inputs and create an Input for each of them. These
    // are then stored in the internal inputs.
    for (int i = 0; i < inputs->size(); i += 2) {
        Input input(i * params->optStep, (*inputs)[i], (*inputs)[i + 1]);
        this->inputs.push_back(input);
    }
}